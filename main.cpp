#include <QCoreApplication>
#include<QDebug>
#include<iostream>
#include<vector>
#include<time.h>
#include<stdlib.h>
using namespace std;


void Oyun();
void zorluksor();
void KazananVarMi();
void YapayZekaHamlesi();
void BoardGoster();
int SayiUret();
int KullaniciGiris();
bool TahtaDolduMu();
int BilgiayarHamlesi();
bool kontrol(int yer);
int rndkontrol(int rnd, int sayi=9);
int rndsayi(int sayi =9);
int çaprazolasılık(int bölge1, int bölge2, int yer);
void AcilisMesaji();

char matrix[7][7];
int index1;
vector<char> konum={'0','1','2','3','4','5','6','7','8','9'};
bool kazananvarmi=false;
bool hamleSayaci=0;
char kazanan='-';
int zorluk ;

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	srand(time(NULL));
	Oyun();


		return a.exec();
}

void BoardGoster()
{
	int i,j,sayac =1;

		for(i=0 ; i<7;i++)
		{
			for(j=0;j<7;j++)
			{
				if(i==0 || i==6 || j==0  || j==6)
				{
					matrix[i][j]='#';
				}
				else if(i==2 || i==4)
				{
					matrix[i][j]='-';
				}
				else if(j==2 || j==4)
				{
					matrix[i][j]='|';
				}
				else
				{
					matrix[i][j]=konum[sayac];
					sayac++;
				}
				cout<<matrix[i][j]<<" ";
			}
			cout<<endl;
		}
		cout<<endl;
}
void zorluksor(){

	cout << endl << "0 - Kolay \n1 - Orta \n2 - Zor" << endl << "Zorluk seçiniz :" ;

	char zorlukseçimi = '3';

	cin >> zorlukseçimi;

	switch(zorlukseçimi){
		case '0':
			zorluk = 0;
			break;
		case '1':
			zorluk = 1;
			break;
		case '2':
			zorluk = 2;
			break;
		default:
			cout << "Hatalı Giriş !" << endl;
			zorluksor();
			break;
	}


}
void KazananVarMi()
{
	int i;
	for(i=1;i<=9;i+=3)
	{

		//yatay konrtol
		if((konum[i]=='X' && konum[i+1]=='X' && konum[i+2]=='X') || (konum[i]=='O' && konum[i+1]=='O' && konum[i+2]=='O') )
		{
			kazananvarmi=true;
			kazanan=konum[i];
			break;

		}
		//dikey kontrol
		if((konum[i]=='X' && konum[i+3]=='X' && konum[i+6]=='X') || (konum[i]=='O' && konum[i+3]=='O' && konum[i+6]=='O'))
		{
			kazananvarmi=true;
			kazanan=konum[i];
			break;

		}
		//çapraz kontrol
		if((konum[1]=='X' && konum[5]=='X' && konum[9]=='X')|| (konum[3]=='X' && konum[5]=='X' && konum[7]=='X'))
		{
			kazananvarmi=true;
			kazanan=konum[5];
			break;

		}
		if((konum[1]=='O' && konum[5]=='O' && konum[9]=='O')|| (konum[3]=='O' && konum[5]=='O' && konum[7]=='O'))
		{
			kazananvarmi=true;
			kazanan=konum[5];
			break;

		}
		if(i==9)//kazanan yoksa
		{
			kazananvarmi=false;
			kazanan='-';

		}
	}

}
int SayiUret()
{
	int rnd;
	while(true)
		{
			rnd=1+rand()%(8);
			if(konum[rnd]!='X' &&  konum[rnd]!='O' )
			{
				break;
			}
			//cout<<rnd<<endl;
		}


	return rnd;
}
void YapayZekaHamlesi()
{
	int hamle;
	if(zorluk==0)
	{
		hamle=SayiUret();
		konum[hamle]='O';
		cout<<"Bilgisayarın hamlesi "<<hamle+1<<" nolu alana O\n"<<endl;
	}
	if(zorluk==1)
	{
		if((konum[5] != 'X' && konum[5] != 'O')){
			// Orta Boşsa :
			konum[5] = 'O';
		}else{
			// Orta Boş Değilse :
			hamle=BilgiayarHamlesi();
			konum[hamle]='O';
			}
	}
	if(zorluk==2)
	{

	}
}


int KullaniciGiris()
{	int kullaniciGirisi;
	cout<<"Hamlenizi yapiniz: ";
	cin>>kullaniciGirisi;

	if(kullaniciGirisi<1 || kullaniciGirisi>9)
	{
		cout<<"Hatali giris yaptiniz!!"<<endl;
		cout<<"Lutfen tekrar giris yapiniz!!"<<endl;
		KullaniciGiris();
	}
	if(konum[kullaniciGirisi]=='X' || konum[kullaniciGirisi]=='O')
	{
		cout<<"Hatali giris yaptiniz!!"<<endl;
		cout<<"Islem yapmak istediginiz alan doludur!!"<<endl;
		cout<<"Lutfen tekrar giris yapiniz!!\n\n"<<endl;
		KullaniciGiris();
	}

	cout<<"Sizin hamleniz "<<kullaniciGirisi<<" nolu alana X"<<endl;
	konum[kullaniciGirisi]='X';
	return kullaniciGirisi;
}

void Oyun()
{
	AcilisMesaji();
	BoardGoster();
	zorluksor();
	while (true)
	{
		if( kazananvarmi==false && TahtaDolduMu() ==true)
		{
			cout<<"\n\n Oyun berabere bitti!!"<<endl;
		}
		KullaniciGiris();
		BoardGoster();
		KazananVarMi();
		if(kazananvarmi==true)
		{
			cout<<kazanan<<"kazandı"<<endl;
			break;
		}
		YapayZekaHamlesi();
		BoardGoster();
		KazananVarMi();
		if(kazananvarmi==true)
		{
			cout<<kazanan<<"kazandı"<<endl;
			break;
		}
	}
}
bool TahtaDolduMu()
{
	int i;
	for(i=1;i<=9;i++)
	{
		if(konum[i]!='X' || konum[i]!='O')
		{
			return false;
		}
	}
	return true;
}
int BilgiayarHamlesi()
{
	// Olasılıklar:

	// Dikey Olasılıklar :

	// 1,2,3 ve 4,5,6 aynı cins doluysa 7,8,9'a koy.
	for(int a = 1,b = 4, c = 7; a <= 3;a++,b++,c++){
		if(((konum[a] == 'X' && konum[b] == 'X') || (konum[a] == 'O' && konum[b] == 'O')) && (konum[c] != 'X' && konum[c] != 'O'))
			return c;
	}

	// 7,8,9 ve 4,5,6 aynı cins doluysa 1,2,3'a koy.
	for(int a = 7,b = 4, c = 1; a <= 9;a++,b++,c++){
		if(((konum[a] == 'X' && konum[b] == 'X') || (konum[a] == 'O' && konum[b] == 'O')) && (konum[c] != 'X' && konum[c] != 'O'))
			return c;
	}

	// 1,2,3 ve 7,8,9 aynı cins doluysa 4,5,6'a koy.
	for(int a = 1,b = 7, c = 4; a <= 3;a++,b++,c++){
		if(((konum[a] == 'X' && konum[b] == 'X') || (konum[a] == 'O' && konum[b] == 'O')) && (konum[c] != 'X' && konum[c] != 'O'))
			return c;
	}


	// Yatay Olasılıklar :


	// 1,4,7 ve 2,4,8 aynı cins doluysa 3,6,9'a koy.

	for(int a = 1; a <= 7; a += 3){
		if(((konum[a] == 'X' && konum[a+1] == 'X') || (konum[a] == 'O' && konum[a+1] == 'O')) && (konum[a+2] != 'X' && konum[a+2] != 'O'))
			return (a+2);
	}

	// 2,5,8 ve 3,6,9 aynı cins doluysa 1,4,7'a koy.

	for(int a = 2; a <= 8; a += 3){
		if(((konum[a] == 'X' && konum[a+1] == 'X') || (konum[a] == 'O' && konum[a+1] == 'O')) && (konum[a-1] != 'X' && konum[a-1] != 'O'))
			return (a-1);
	}

	// 1,4,7 ve 3,6,9 aynı cins doluysa 2,5,8'a koy.

	for(int a = 1; a <= 7; a += 3){
		if(((konum[a] == 'X' && konum[a+2] == 'X') || (konum[a] == 'O' && konum[a+2] == 'O')) && (konum[a+1] != 'X' && konum[a+1] != 'O'))
			return (a+1);
	}


	// Çapraz Olasılıklar :

	// 1 - 5 -> 9
	if(çaprazolasılık(1,5,9) != 0)
		return çaprazolasılık(1,5,9);

	// 3 - 5 -> 7
	if(çaprazolasılık(3,5,7) != 0)
		return çaprazolasılık(3,5,7);

	// 5 - 9 -> 1
	if(çaprazolasılık(5,9,1) != 0)
		return çaprazolasılık(5,9,1);

	// 7 - 5 -> 3
	if(çaprazolasılık(7,5,3) != 0)
		return çaprazolasılık(7,5,3);

	// 1 - 9 -> 5
	if(çaprazolasılık(1,9,5) != 0)
		return çaprazolasılık(1,9,5);

	// 3 - 7 -> 5
	if(çaprazolasılık(3,7,5) != 0)
		return çaprazolasılık(3,7,5);


	// Eğer zor seçiliyse ve bir olasılık yoksa köşeye oyna :

	int rnd = rndkontrol(rndsayi(4));

	switch(rnd){

		case 1:
			return 1;
			break;
		case 2:
			return 3;
			break;
		case 3:
			return 7;
			break;
		case 4:
			return 9;
			break;
		default:
			cout << "Bir Hata Oluştu : Eğer zor seçiliyse ve bir olasılık yoksa köşeye oyna." << endl;
			break;

	}

	// Hiçbir olasılık yoksa rastgele seç :
	return rndkontrol(rndsayi());;

}
int rndsayi(int sayi){

	return (rand() % sayi + 1);

}

int rndkontrol(int rnd, int sayi ){

	if(kontrol(rnd)){

		return rnd;

	}else{

		return (rndkontrol(rndsayi(sayi)));

	}

}

int çaprazolasılık(int bölge1, int bölge2, int yer){

	if(((konum[bölge1] == 'X' && konum[bölge2] == 'X') || (konum[bölge1] == 'O' && konum[bölge2] == 'O')) && (konum[yer] != 'X' && konum[yer] != 'O'))
		return yer;

	// Uygun yer bulunamadıysa :
	return 0;
}
bool kontrol(int yer){

	return ((konum[yer] != 'X' && konum[yer] != 'O' ? true : false));

}
void AcilisMesaji()
{
	cout << endl << "-----------------Tic Tac Toe------------- " << endl;
}
